<?php
session_start();
if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {
	header('Access-Control-Allow-Headers: origin, X-Requested-With, Authorization, Content-Type');
	header('Access-Control-Allow-Methods: POST, GET, OPTIONS');
	// exit;
}
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Headers: origin, X-Requested-With, Authorization, Content-Type');
header('Access-Control-Allow-Methods: POST, GET, OPTIONS');
header('Content-type: application/json');

$jsonresponse = new stdClass();

$r = array(
	array(
		"provider_code"=>"010130089-01",
		"provider_name"=>"QUALIMED TRINOMA",
		"longitude"=>"121.04393",
		"latitude"=>"14.634297",
		"city"=>"QUEZON CITY",
		"province"=>"METRO MANILA",
		"region"=>"NCR",
		"zip_code"=>"1100",
		"clinic_complete_address"=>"Ground Floor Level, Trinoma, North Avenue cor EDSA, Quezon City",
		"clinic_contact_no"=>"",
		"type"=>""
	)
	);
    $jsonresponse->success = true;
    $jsonresponse->message ="YEY";
    $jsonresponse->total = "0";
    $jsonresponse->data = $r;

    echo json_encode($jsonresponse);


?>